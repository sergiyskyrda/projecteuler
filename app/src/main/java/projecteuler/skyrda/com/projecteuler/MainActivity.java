package projecteuler.skyrda.com.projecteuler;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import projecteuler.skyrda.com.projecteuler.problems.BaseProblem;
import projecteuler.skyrda.com.projecteuler.problems.problem2.Problem2;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        solveProblems();
    }

    private void solveProblems() {
        BaseProblem problem = new Problem2();
        problem.solve();
    }
}
