package projecteuler.skyrda.com.projecteuler.problems.problem1;

import android.util.Log;

import projecteuler.skyrda.com.projecteuler.problems.BaseProblem;

public class Problem1 implements BaseProblem {

    @Override
    public void solve() {
        int sumFor3 = sumOfAryphmeticProgression(3, 3, 333);
        int sumFor5 = sumOfAryphmeticProgression(5, 5, 199);
        int sumFor15 = sumOfAryphmeticProgression(15, 15, 66);
        int result = sumFor3 + sumFor5 - sumFor15;
        Log.e("RESULT", String.valueOf(result));
    }

    private int sumOfAryphmeticProgression(int firstElement, int step, int amount) {
        return (2 * firstElement + step * (amount - 1)) * amount / 2;
    }
}
