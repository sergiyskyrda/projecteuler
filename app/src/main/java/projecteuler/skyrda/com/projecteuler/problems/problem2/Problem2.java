package projecteuler.skyrda.com.projecteuler.problems.problem2;

import android.util.Log;

import projecteuler.skyrda.com.projecteuler.problems.BaseProblem;

public class Problem2 implements BaseProblem {

    @Override
    public void solve() {
        int maxValue = 4000000;

        int first = 1;
        int second = 2;
        int sum = first + second;
        int evenCounter = 0;
        int result = second;
        while (sum <= maxValue) {
            if (evenCounter == 2) {
                result += sum;
                evenCounter = 0;
            } else {
                evenCounter++;
            }
            first = second;
            second = sum;
            sum = first + second;
        }
        Log.e("RESULT", String.valueOf(result));
    }

}
