package projecteuler.skyrda.com.projecteuler.problems;

public interface BaseProblem {
    void solve();
}
